Hooks.on("ready", function() {
  
  console.log("Setting system settings for Homebrew World");

  game.settings.set('dungeonworld', 'xpFormula', 5)
  
  game.settings.set('dungeonworld', 'alignmentSingle', 'Drive')
  game.settings.set('dungeonworld', 'alignmentPlural', 'Drives')
  game.settings.set('dungeonworld', 'raceSingle', 'Background')
  game.settings.set('dungeonworld', 'racePlural', 'Backgrounds')
  game.settings.set('dungeonworld', 'bondSingle', 'Background Question')
  game.settings.set('dungeonworld', 'bondPlural', 'Background Questions')
  
  game.settings.set('dungeonworld', 'noCompendiumAutoData', true)
  game.settings.set('dungeonworld', 'compendiumPrefix', 'hbw')
  
  game.settings.set('dungeonworld', 'noAbilityScores', true)
  game.settings.set('dungeonworld', 'noAbilityIncrease', true)
  game.settings.set('dungeonworld', 'noConstitutionToHP', true)
  game.settings.set('dungeonworld', 'noSTRToMaxLoad', true)
  
  game.settings.set('dungeonworld', 'debilityLabelSTR', 'Weakened')
  game.settings.set('dungeonworld', 'debilityLabelDEX', 'Weakened')
  game.settings.set('dungeonworld', 'debilityLabelCON', 'Miserable')
  game.settings.set('dungeonworld', 'debilityLabelINT', 'Dazed')
  game.settings.set('dungeonworld', 'debilityLabelWIS', 'Dazed')  
  game.settings.set('dungeonworld', 'debilityLabelCHA', 'Miserable')
  
});
